# EDuke32 CON Syntax Highligthing for Sublime Text 3

## About
There are few keywords missing, notably the ones that were introduced down the line of Eduke's improvement, however all keyowrds present and usable in Duke3D should be available

## How to Use

1. Open up Sublime
2. Preferences->Browse Packages
3. Open up the `User` folder
4. put `edukecon.sublime-syntax` in the User folder
